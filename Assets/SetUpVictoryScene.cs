﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetUpVictoryScene : MonoBehaviour
{
    public Text scoreText;
    public Text moveUsed;
    public Text totalNumberOfPieces;
    // Use this for initialization
    void Start ()
    {
        KeepScore keepScore = GameObject.FindObjectOfType<KeepScore>();
        ValueStorage valustorage = GameObject.FindObjectOfType<ValueStorage>();
        int score = keepScore.GetScore;
        scoreText.text = score.ToString();

        string moveUsedText = moveUsed.text;
        string numberofmoveUsed = keepScore.GetMoveUsed.ToString();
        moveUsedText = moveUsedText.Replace("XX", numberofmoveUsed);
        moveUsed.text = moveUsedText;

        string numberOfPiecesText = totalNumberOfPieces.text;
        string numberOfPieces = valustorage.numberOfPieces.ToString();
        numberOfPiecesText = numberOfPiecesText.Replace("XX", numberOfPieces);
        totalNumberOfPieces.text = numberOfPiecesText;
	}
}
