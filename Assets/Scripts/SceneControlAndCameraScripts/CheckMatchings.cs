﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class CheckMatchings : MonoBehaviour
{

    private List<PieceSelectAndAnimation> currentPiece = new List<PieceSelectAndAnimation>();
    private KeepScore keepScore;
    private ValueStorage valueStorage;
    private int numberOfPiecesLeft;

    void Start()
    {
        keepScore = GetComponent<KeepScore>();
        valueStorage = GetComponent<ValueStorage>();
        if (keepScore == null)
        {
            Debug.LogError(gameObject.name + " is missing the Keep Score Script please add it", gameObject);
        }
        if (valueStorage == null)
        {
            Debug.LogError(gameObject.name + "cant find the valueStorage please make certain that it is setup Correctly");
        }
        numberOfPiecesLeft = valueStorage.numberOfPieces;
    }

    public PieceSelectAndAnimation CheckAPiece
    {
        set
        {
            for (int i = 0; i < currentPiece.Count; i++)
            {
                if (value == currentPiece[i])
                {
                    return;
                }
            }
            currentPiece.Add(value);
            CheckCurrentPiece();
        }
    }

    private void CheckCurrentPiece()
    {
        if (currentPiece.Count <= 1)
        {
            return;
        }

        Texture textureA = currentPiece[0].faceTexture;
        Texture textureB = currentPiece[1].faceTexture;
        if (textureA == textureB)
        {
            currentPiece[0].DestroyIt();
            currentPiece[1].DestroyIt();
            currentPiece.Clear();
            keepScore.ChangeScore(true);
            numberOfPiecesLeft -= 2;
        }
        else
        {
            currentPiece[0].DeselectIt();
            currentPiece[1].DeselectIt();
            keepScore.ChangeScore(false);
            currentPiece.Clear();
        }
        if (numberOfPiecesLeft == 0)
        {
            SceneManager.LoadScene("VictoryScene",LoadSceneMode.Additive);
        }
    }
}
