﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Loads in the textures in the Resources Folder and stores them in an Array
/// </summary>
public class LoadTextures
{
	// Use this for initialization
	public Texture2D[] getTextures ()
    {
        Texture2D[] loadedTextures;
        string imagePass = "Textures";
        Object[] textures = Resources.LoadAll(imagePass);
        loadedTextures = new Texture2D[textures.Length];
        for (int i = 0; i < textures.Length; i++)
        {
            loadedTextures[i] = (Texture2D)textures[i];
        }
        return loadedTextures;
	}
}
