﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class KeepScore : MonoBehaviour
{
    public Text scoreNumber;
    int score = 0;
    int moveUsed = 0;
    public int positivScoreChange = 3;
    public int negativScoreChange = -2;

    public int GetScore { get { return score; } }
    public int GetMoveUsed { get { return moveUsed; } }

    void Start()
    {
        if (scoreNumber == null)
        {
            Debug.LogError(gameObject.name + " the Script KeepScore needs to know which Unity Text should get the Score Just add a UI Unity Text to the public Object", gameObject);
            return;
        }
        scoreNumber.text = score.ToString();
    }

    public void ChangeScore(bool matching)
    {
        int scoreChange = 0;
        if (matching == true)
        {
            scoreChange = positivScoreChange;
        }
        else
        {
            scoreChange = negativScoreChange;
        }
        score += scoreChange;
        moveUsed += 2;
        scoreNumber.text = score.ToString();
    }
}
