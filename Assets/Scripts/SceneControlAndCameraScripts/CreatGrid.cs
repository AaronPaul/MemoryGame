﻿using UnityEngine;
using System;
using System.Collections.Generic;
/// <summary>
/// Creats the Gridpositions on which the Game Pieces will be placed 
/// and returns it and all nessesary Values in a List of the GridPosition Class
/// </summary>
public class CreatGrid
{
	public List<GridPosition> getTheGrid(int numberOfTextures, float distanceBetwenPieces)
    {
        List<GridPosition> gridpositions = new List<GridPosition>();
        int numberOfPieces = numberOfTextures * 2;
        float rowlength = Mathf.Sqrt(numberOfPieces);
        rowlength = (float)Math.Ceiling(rowlength);

        float heightLenght = numberOfPieces / rowlength;
        heightLenght = (float)Math.Ceiling(heightLenght);

        float currentpositionRow = 0;
        float currentpositionHeight = 0;
        for (int i = 0; i < numberOfPieces; i++)
        {
            GridPosition currentGridPosition = new GridPosition();
            if (currentpositionRow >= rowlength)
            {
                currentpositionRow = 0;
                currentpositionHeight += 1;
            }
            float rowOffset = 0;
            if (currentpositionHeight == heightLenght-1) //ask if the current Height is the last one and centers the remaining pieces
            { 
                float remainigPieces = numberOfPieces - (((heightLenght-1) * rowlength));
                rowOffset = (rowlength - remainigPieces) / 2;
            }
            float targetRowPos = currentpositionRow + rowOffset;
            float targetHeightPos = currentpositionHeight;
            float spawnRowPos = targetRowPos + targetRowPos * distanceBetwenPieces;
            float spawnHeightPos = targetHeightPos + targetHeightPos * distanceBetwenPieces;

            Vector3 spawnPosition = Vector3.zero;
            spawnPosition.x = spawnRowPos;
            spawnPosition.y = spawnHeightPos;

            currentGridPosition.spawnPosition = spawnPosition;
            currentGridPosition.heightLength = heightLenght;
            currentGridPosition.rowLength = rowlength;
            currentGridPosition.offsetPosition = new Vector2(targetRowPos, targetHeightPos);
            gridpositions.Add(currentGridPosition);

            currentpositionRow++;
        }

        return gridpositions;
    }
}
