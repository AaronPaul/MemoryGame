﻿using UnityEngine;
using System;

public class ValueStorage : MonoBehaviour {

    public static ValueStorage Instance { get; private set; }

    public Vector3 minBoundary = Vector3.zero;
    public Vector3 maxBoundary = Vector3.zero;
    public int numberOfPieces = 0;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        Instance = this;
    }

}
