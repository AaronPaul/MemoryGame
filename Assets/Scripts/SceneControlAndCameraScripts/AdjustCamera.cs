﻿using UnityEngine;
using UnityEngine.Events;

public class AdjustCamera : MonoBehaviour
{
    /// <summary>
    /// This Scripts find the Center of the Puzzle then 
    /// moves the Camera so that it always looks at the Whole Puzzle.
    /// </summary>
    ValueStorage cameraBoundary;
    Vector3 minPos;
    Vector3 maxPos;
    Vector3 centerPos;
    Camera mainCam;
    float speed = 4;
    float distance = 0;
    bool finishedZoom = true;

    private UnityAction eventListener;

    // Use this for initialization
    void Awake ()
    {
        eventListener = new UnityAction(StartZoom);
    }

    void OnEnable()
    {
        EventManager.StartListing("ZoomOut", eventListener);
    }

    void OnDisable()
    {
        EventManager.StopListening("ZoomOut", eventListener);
    }
    private void StartZoom()
    {
        FindAndSetCenter();
        finishedZoom = false;
    }

    private void FindAndSetCenter()
    {
        cameraBoundary = FindObjectOfType<ValueStorage>();
        mainCam = Camera.main;
        if (cameraBoundary == null)
        {
            Debug.Log("SceneManager Cannot be found", gameObject);
        }
        minPos = cameraBoundary.minBoundary;
        maxPos = cameraBoundary.maxBoundary;

        float centerX = (minPos.x + maxPos.x) / 2;
        float centerY = (minPos.y + maxPos.y) / 2;
        float centerZ = (minPos.z + maxPos.z) / 2 + distance;
        centerPos = new Vector3(centerX, centerY, centerZ);

        transform.position = centerPos;

    }

    // Update is called once per frame. It Zooms out till it views all the GamePieces. 
    void Update()
    {
        ZoomOut(); 
    }

    private void ZoomOut()
    {
        if (finishedZoom == true)
        {
            return;
        }
        Vector3 viewPos = mainCam.WorldToViewportPoint(maxPos);
        float viewPosX = viewPos.x;
        float viewPosY = viewPos.y;
        int numberOfPieces = cameraBoundary.numberOfPieces;
        float cameraOffset = 0.1f - 0.1f / (float)numberOfPieces;
        float viewPosMax = 1f - cameraOffset;
        if (viewPosX > viewPosMax || viewPosY > viewPosMax)
        {
            Vector3 camPos = transform.position;
            camPos.z -= 1;
            transform.position = Vector3.Lerp(transform.position, camPos, speed * Time.deltaTime);
            return;
        }
        finishedZoom = true;
    }
}
