﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// Can only be used once then listens to the event of the Menu Closing 
/// to reset the bool and to be able to be used again. 
/// </summary>

public class ActivatePauseMenu : MonoBehaviour
{
    public bool menuOpen = false;
    GameObject eventSystem = null;
    private UnityAction eventListener;
	// Use this for initialization
	void Awake ()
    {
        eventListener = new UnityAction(HasClosedPauseMenu);
        eventSystem = GameObject.Find("EventSystem");
    }

    private void HasClosedPauseMenu()
    {
        menuOpen = false;
        eventSystem.SetActive(true);
    }

    void OnEnable()
    {
        EventManager.StartListing("CloseMenu", eventListener);
    }

    void OnDisable()
    {
        EventManager.StopListening("CloseMenu", eventListener);
    }
    // Update is called once per frame
    void Update ()
    {

        if (menuOpen == true)
        {
            return;
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu",LoadSceneMode.Additive);
            eventSystem.SetActive(false);
            menuOpen = true;
        }
	}
}
