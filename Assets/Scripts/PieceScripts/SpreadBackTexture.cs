﻿using UnityEngine;
using System.Collections;

public class SpreadBackTexture : MonoBehaviour
{
    public float rowLength = 0;
    public float heightLength = 0;

    public GridPosition gridPosition;
    public Vector2 offsetPosition = Vector2.zero;

    public float rowTilling = 0;
    public float columTilling = 0;
    public float rowOffset = 0;
    public float columOffset = 0;
    public GameObject backGroundTexture;
    // Use this for initialization
    void Start()
    {
        rowLength = gridPosition.rowLength;
        heightLength = gridPosition.heightLength;
        offsetPosition = gridPosition.offsetPosition;
        rowTilling = 1f / rowLength;
        columTilling = 1f / heightLength;

        rowOffset = rowTilling * offsetPosition.x;
        columOffset = columTilling * offsetPosition.y;

        Material backgroundMat = backGroundTexture.GetComponent<Renderer>().material;
        backgroundMat.SetTextureScale("_MainTex", new Vector2(rowTilling, columTilling));
        backgroundMat.SetTextureOffset("_MainTex", new Vector2(rowOffset, columOffset));
    }

    // Update is called once per frame
    void Update()
    {

    }
}
