﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
/// <summary>
/// Spawns the pieces. 
/// </summary>
public class SpawnThePieces : MonoBehaviour
{
    public float distanceBetwenPieces = 0.5f;
    public Transform spawnPositionMarker;
    Vector3 spawnposition;
    Texture2D[] textures;
    LoadTextures loadTextures = new LoadTextures();
    CreatGrid creatGrid = new CreatGrid();

    List<GameObject> spawnedPieces = new List<GameObject>();
    List<GameObject> untexturedPieces = new List<GameObject>();
    public ValueStorage valueStorage;
    
    /// <summary>
    /// reads out the textures in the Resources/texture folder(loadTextures).
    /// Then calculates a grid (CreataGrid) depending on the ammount of Textures found there.
    /// After that it spawns pieces on each of the grid Positions (SpawnToGrid)
    /// and gives it textures accordingly. (SetTexture).
    /// Lastly it calculates teh Boundary for the Camera so it can View them all. (FindTheOuterBondary)
    /// </summary>
    void Awake ()
    {
        spawnposition = spawnPositionMarker.position;
        textures = loadTextures.getTextures();
        int numberOfTextures = textures.Length;
        List<GridPosition> gridSpawnPositions = new List<GridPosition>();
        gridSpawnPositions = creatGrid.getTheGrid(numberOfTextures, distanceBetwenPieces);

        SpawnToGrid(gridSpawnPositions);
        SetTexture();
        FindTheOuterBondary();
    }

    void Start()
    {
        EventManager.TriggerEvent("ZoomOut");
    }
    //spawns the Pieces at the position calculated in the creat grid function.
    private void SpawnToGrid(List<GridPosition> gridPosition)
    {
        for (int i = 0; i < gridPosition.Count; i++)
        {
            GameObject piece = (GameObject)Instantiate(Resources.Load("Piece"), transform.position, Quaternion.identity);
            Vector3 gridSpawnPosition = gridPosition[i].spawnPosition;
            piece.transform.position = gridSpawnPosition + spawnposition;
            piece.name = "Piece " + i.ToString();
            spawnedPieces.Add(piece);

            SpreadBackTexture pieceBackground = piece.GetComponent<SpreadBackTexture>();
            pieceBackground.gridPosition = gridPosition[i];
        }

    }

    //runs GiveTexture two times so two pieces are getting the same Texture.
    private void SetTexture()
    {
        untexturedPieces = new List<GameObject>(spawnedPieces);

        for (int i = 0; i < textures.Length; i++)
        {
            GiveTexture(i);
            GiveTexture(i);
        }
    }
    //gives the Textures that where loaded into the game earlier
    private void GiveTexture(int i)
    {
        int number = UnityEngine.Random.Range(0, untexturedPieces.Count);
        PieceSelectAndAnimation piece = untexturedPieces[number].GetComponent<PieceSelectAndAnimation>();
        Material material = piece.puzzlePieceTexture.GetComponent<Renderer>().material;
        material.SetTexture("_MainTex", textures[i]);
        piece.faceTexture = textures[i];
        untexturedPieces.Remove(untexturedPieces[number]);
    }
    /// <summary>
    ///Finds the lowest left point and highest right point off the Spawned pieces by asking the indivual Min/Max 
    ///of the bounary of the currently Spawned piece is and gives them to the storage Value Script so the Camera
    ///knows how far to Zoom out.
    /// </summary>
    private void FindTheOuterBondary()
    {
        Vector3 lowtarget = Vector3.zero;
        Vector3 hightarget = Vector3.zero;
        for (int i = 0; i < spawnedPieces.Count; i++)
        {
            Vector3 minBound = spawnedPieces[i].GetComponent<Collider>().bounds.min;
            Vector3 maxBound = spawnedPieces[i].GetComponent<Collider>().bounds.max;
            if (i == 0)
            {
                lowtarget = spawnedPieces[i].GetComponent<Collider>().bounds.min;
                hightarget = spawnedPieces[i].GetComponent<Collider>().bounds.max;
            }

            if (minBound.x < lowtarget.x)
            {
                lowtarget.x = minBound.x;
            }
            if (minBound.y < lowtarget.y)
            {
                lowtarget.y = minBound.y;
            }
            if (minBound.z < lowtarget.z)
            {
                lowtarget.z = minBound.z;
            }

            if (maxBound.x > hightarget.x)
            {
                hightarget.x = maxBound.x;
            }
            if (maxBound.y > hightarget.y)
            {
                hightarget.y = maxBound.y;
            }
            if (maxBound.z > hightarget.z)
            {
                hightarget.z = maxBound.z;
            }
        }

        valueStorage.minBoundary = lowtarget;
        valueStorage.maxBoundary = hightarget;
        valueStorage.numberOfPieces = spawnedPieces.Count;
    }
}
