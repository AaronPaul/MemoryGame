﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Just a simple struct that transfares the Data to the newly generated Objects.
/// the Spawnposition sets its position on the World Grid
/// the gridposition, heightlength and rowlength are used to spread the BackTexture evenly
/// </summary>
public struct GridPosition
{
    public Vector3 spawnPosition { get; set; }
    public Vector2 offsetPosition { get; set; }
    public float heightLength { get; set; }
    public float rowLength { get; set; }
}
