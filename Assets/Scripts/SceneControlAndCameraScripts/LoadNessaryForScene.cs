﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

/// <summary>
/// Load this in a Scene so that all nessary Scenes are loaded even if you dont go from the SetupScene
/// </summary>
public class LoadNessaryForScene : MonoBehaviour 
{
    private void Awake ()
    {
        EventManager eventManager = GameObject.FindObjectOfType<EventManager>();
        if (Camera.main && eventManager)
        {
            return;
        }
        
        if (Camera.main == null)
        {
            SceneManager.LoadScene("CameraScene", LoadSceneMode.Additive);
        }
        if (eventManager == null )
        {
            GameObject EventManger = (GameObject)Instantiate(Resources.Load("GameEventManager"), Vector3.zero, Quaternion.identity);
        }
    }
}
