﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

/// <summary>
/// Creats a EventSystem that allows to creat custom Events. The EventSystem needs to be on a Empty GameObject.
/// Credits for the eventsytem: Adam Buckner from Unity ((the rest is all mine))
/// </summary>

public class EventManager : MonoBehaviour
{
    private Dictionary<string, UnityEvent> eventDictornay;

    public static EventManager eventManager;

    public static EventManager SingletonInstance { get; private set; }

    public static EventManager instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                if (!eventManager)
                {
                    Debug.LogError("There needs to be one active EventManaer script on a GameObject in your scene");
                    
                }
                else
                {
                    eventManager.Init();
                }
            }
            return eventManager;
        }
    }

    void Init()
    {
        if (eventDictornay == null)
        {
            eventDictornay = new Dictionary<string, UnityEvent>();
        }
    }

    public static void StartListing (string eventName, UnityAction listener)
    {
        UnityEvent thisEvent = null;
        if (instance.eventDictornay.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            instance.eventDictornay.Add(eventName, thisEvent);
        }
    }
    public static void StopListening (string eventName, UnityAction listener)
    {
        if (eventManager == null)
        {
            return;
        }
        UnityEvent thisEvent = null;
        if (instance.eventDictornay.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent (string eventName)
    {
        UnityEvent thisEvent = null;
        if (instance.eventDictornay.TryGetValue (eventName, out thisEvent))
        {
            thisEvent.Invoke();
        }
    }

    //makes certain that it is only one Eventsystem in the Scene;
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        if (SingletonInstance != null && SingletonInstance != this)
        {
            Destroy(gameObject);
        }

        SingletonInstance = this;
    }
}
