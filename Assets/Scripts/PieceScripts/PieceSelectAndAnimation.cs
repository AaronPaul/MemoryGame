﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

public class PieceSelectAndAnimation : MonoBehaviour
{
    /// <summary>
    /// this script activates the Animations of the Puzzle Piece 
    /// and keeps track of if the piece has been clicked;
    /// 
    /// Written by Aaron Schweighöfer
    /// </summary>
    Animator puzzlePieceAnimator;
    CheckMatchings checkPiece;
    public GameObject puzzlePieceTexture;
    public Texture faceTexture;
    public bool activated = false;

    int activeEvents = 0;
    private UnityAction eventListener;
    // Use this for initialization

    void Awake()
    {
        eventListener = new UnityAction(AddActivePieces);
    }

    private void AddActivePieces()
    {
        activeEvents++;
    }

    private void RemoveActivePieces()
    {
        activeEvents--;
    }

    void OnEnable()
    {
        EventManager.StartListing("AddPieces", eventListener);
        EventManager.StartListing("RemovePieces", RemoveActivePieces);
    }

    void OnDisable()
    {
        EventManager.StopListening("AddPieces", eventListener);
        EventManager.StopListening("RemovePieces", RemoveActivePieces);
    }

    void Start()
    {
        puzzlePieceAnimator =  GetComponentInChildren<Animator>();
        checkPiece = FindObjectOfType<CheckMatchings>();
        if (checkPiece == null)
        {
            Debug.Log("no SceneManager Found", gameObject);
            GetComponent<PieceSelectAndAnimation>().enabled = false;
        }
        if (puzzlePieceAnimator == null)
        {
            Debug.Log("This piece is missing a Animator in the ChildObject", gameObject);
            GetComponent<PieceSelectAndAnimation>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (activated == false)
        {
            return;
        }
        if (puzzlePieceAnimator.GetCurrentAnimatorStateInfo(0).IsName("Hold"))
        {
            checkPiece.CheckAPiece = gameObject.GetComponent<PieceSelectAndAnimation>();
        }

    }

    void OnMouseEnter()
    {
        puzzlePieceAnimator.SetBool("MouseOver", true);
    }

    void OnMouseExit()
    {
        puzzlePieceAnimator.SetBool("MouseOver", false);
    }

    void OnMouseDown()
    {
        if (activated == true)
        {
            return;
        }
        if (activeEvents > 1)
        {
            return;
        }
        puzzlePieceAnimator.SetBool("Selected", true);
        puzzlePieceAnimator.SetBool("MouseOver", false);
        activated = true;
        EventManager.TriggerEvent("AddPieces");
    }

    public void DestroyIt()
    {
        EventManager.TriggerEvent("RemovePieces");
        Destroy(gameObject);
    }

    public void DeselectIt()
    {
        activated = false;
        EventManager.TriggerEvent("RemovePieces");
        puzzlePieceAnimator.SetBool("Selected", false);
    }
}
