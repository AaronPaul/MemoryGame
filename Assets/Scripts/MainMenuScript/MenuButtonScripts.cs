﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Controls the Buttons and checks if the MainMenuScene has everything it needs. 
/// </summary>
public class MenuButtonScripts : MonoBehaviour
{
    public GameObject continueButton;

    private void Awake()
    {
        if (continueButton == null)
        {
            return;
        }

        if (GameObject.FindObjectOfType<SpawnThePieces>())
        {
            continueButton.SetActive(true);
        }
        else
        {
            continueButton.SetActive(false);
        }
    }

    public void NewGame()
    {
        SceneManager.LoadScene("MainGameScene");
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    public void ContinueButton()
    {
        SceneManager.UnloadSceneAsync("MainMenu");
        EventManager.TriggerEvent("CloseMenu");
    }
}
